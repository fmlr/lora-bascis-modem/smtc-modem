/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2021 Miromico AG
 * All rights reserved.
 */

#ifndef __BSP_I2C_H__
#define __BSP_I2C_H__

#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * --- DEPENDENCIES ------------------------------------------------------------
 */

// #include "smtc_bsp_types.h"
#include "smtc_hal_gpio.h"

/*
 * -----------------------------------------------------------------------------
 * --- PUBLIC MACROS -----------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * --- PUBLIC CONSTANTS --------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * --- PUBLIC TYPES ------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * --- PUBLIC FUNCTIONS PROTOTYPES ---------------------------------------------
 */

/*!

 */
void bsp_i2c_init( const uint32_t id, const hal_gpio_pin_names_t scl, const hal_gpio_pin_names_t sda);


void bsp_i2c_deinit( const uint32_t id );

/*!
 * Sends out_data and receives in_data
 *
 * \param [IN] id       SPI interface id [1:N]
 * \param [IN] out_data Byte to be sent

 * \retval in_data      Received byte.
 */
//uint16_t bsp_i2c_in_out( const uint32_t id, const uint16_t out_data );

void bsp_i2c_txrx (const uint32_t id, uint16_t DevAddress, uint8_t *txData, uint16_t txSize,
        uint8_t *rxData, uint16_t rxSize, uint32_t Timeout);

#ifdef __cplusplus
}
#endif

#endif  // __BSP_I2C_H__
