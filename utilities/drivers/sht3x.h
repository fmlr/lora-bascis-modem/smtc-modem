/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2021 Miromico AG
 * All rights reserved.
 */

#ifndef _SHT3X_H_
#define _SHT3X_H_

#include "driver_types.h"

typedef struct {
    int32_t temp;
    int32_t rh;
} sht3x_data;

void sht3x_init(eDeviceStatus_t* pstatus);

void sht3x_read(eDeviceStatus_t* pstatus, sht3x_data* pdata);

#endif
