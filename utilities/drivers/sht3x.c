/*    __  __ _____ _____   ____  __  __ _____ _____ ____
 *   |  \/  |_   _|  __ \ / __ \|  \/  |_   _/ ____/ __ \
 *   | \  / | | | | |__) | |  | | \  / | | || |   | |  | |
 *   | |\/| | | | |  _  /| |  | | |\/| | | || |   | |  | |
 *   | |  | |_| |_| | \ \| |__| | |  | |_| || |___| |__| |
 *   |_|  |_|_____|_|  \_\\____/|_|  |_|_____\_____\____/
 *
 * Copyright (c) 2021 Miromico AG
 * All rights reserved.
 */
// #include "smtc_bsp_dbg_trace.h"
// #include "hw_modem_pin_names.h"
#include "sht3x.h"
#include "smtc_hal_dbg_trace.h"
#include "example_options.h"
#include "smtc_bsp_i2c.h"

static struct {
    unsigned char buf[6];

    eDeviceStatus_t* pstatus;
    sht3x_data* pdata;
} sht3x;

#define SHT3X_ADDRESS_L   0x44
#define SHT3X_ADDRESS_H   0x45

static const uint8_t SHT3X_ADDRESS = (SHT3X_ADDRESS_H << 1);

static const uint8_t CMD_READ_STATUS_REG[] = { 0xF3, 0x2D };
static const uint8_t CMD_MEASURE_HPM[]     = { 0x24, 0x00 };
static const uint8_t DELAY_MEASURE_HPM     = 15000; //15;

static uint8_t sht3x_generate_crc(uint8_t* data, uint16_t count);
static inline bool sht3x_check_crc(uint8_t* data, uint16_t count);

#define I2C_TIMEOUT   (500)

static void init_func() {
    // SMTC_HAL_TRACE_PRINTF("init_func\n");

    sht3x.buf[0] = CMD_READ_STATUS_REG[0];
    sht3x.buf[1] = CMD_READ_STATUS_REG[1];

    bsp_i2c_txrx(1, SHT3X_ADDRESS, sht3x.buf, 2, sht3x.buf, 3, I2C_TIMEOUT);

    if (sht3x_check_crc(sht3x.buf, 2)) {
        SMTC_HAL_TRACE_PRINTF("eDevice_Initialized\n");
        *sht3x.pstatus = eDevice_Initialized;
    } else {
        SMTC_HAL_TRACE_PRINTF("eDevice_Initialized fail\n");
        *sht3x.pstatus = eDevice_Failed;
    }
}

static void read_func() {
    // SMTC_HAL_TRACE_PRINTF("read_func\n");

    // Trigger measurement
    sht3x.buf[0] = CMD_MEASURE_HPM[0];
    sht3x.buf[1] = CMD_MEASURE_HPM[1];

    bsp_i2c_txrx(1, SHT3X_ADDRESS, sht3x.buf, 2, sht3x.buf, 0, I2C_TIMEOUT);
    // wait for conversion to complete

    // HAL_Delay(DELAY_MEASURE_HPM);
    hal_mcu_wait_us( DELAY_MEASURE_HPM );

    bsp_i2c_txrx(1, SHT3X_ADDRESS, sht3x.buf, 0, sht3x.buf, 6, I2C_TIMEOUT);


    if (sht3x_check_crc(sht3x.buf, 2) && sht3x_check_crc(sht3x.buf + 3, 2)) {
            // SMTC_HAL_TRACE_PRINTF("CRC check ok\n");

        int32_t temp_ticks = (sht3x.buf[1] & 0xff) | ((int32_t)sht3x.buf[0] << 8);
        int32_t rh_ticks = (sht3x.buf[4] & 0xff) | ((int32_t)sht3x.buf[3] << 8);

        /**
        * formulas for conversion of the sensor signals, optimized for fixed point algebra:
        * Temperature       = 175 * S_T / 2^16 - 45
        * Relative Humidity = 100 * S_RH / 2^16
        */
        sht3x.pdata->temp = ((21875 * temp_ticks) >> 13) - 45000;
        sht3x.pdata->rh = ((12500 * rh_ticks) >> 13);
        // all done
        *sht3x.pstatus = eDevice_Ok;
    } else {
        SMTC_HAL_TRACE_PRINTF("CRC fail\n");
        *sht3x.pstatus = eDevice_Failed;
    }
}


static const uint8_t CRC_POLYNOMIAL    = 0x31;
static const uint8_t CRC_INIT          = 0xff;

static inline bool sht3x_check_crc(uint8_t* data, uint16_t count) {
    return sht3x_generate_crc(data, count) == data[count];
}

static uint8_t sht3x_generate_crc(uint8_t* data, uint16_t count) {
    uint8_t crc = CRC_INIT;
    uint8_t current_byte;
    uint8_t crc_bit;

    /* calculates 8-Bit checksum with given polynomial */
    for (current_byte = 0; current_byte < count; ++current_byte) {
        crc ^= (data[current_byte]);
        for (crc_bit = 8; crc_bit > 0; --crc_bit) {
            if (crc & 0x80) {
                crc = (crc << 1) ^ CRC_POLYNOMIAL;
            } else {
                crc = (crc << 1);
            }
        }
    }
    return crc;
}

void sht3x_init(eDeviceStatus_t* pstatus) {
    sht3x.pstatus = pstatus;
    init_func();
}

void sht3x_read(eDeviceStatus_t* pstatus, sht3x_data* pdata) {
    sht3x.pstatus = pstatus;
    sht3x.pdata = pdata;
    read_func();
}
