#-----------------------------------------------------------------------------
# Compilation flags
#-----------------------------------------------------------------------------

# Default MCU compilation flags
CPU = -mcpu=cortex-m0

# If MCU is defined in caller Makefile, use it instead
MCU ?= $(CPU) -mthumb $(FPU) $(FLOAT-ABI)

